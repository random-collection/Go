import os
import pickle

from copy import deepcopy
from sys import exit


class GoParent(object):

	def __init__(self, file_path, new_game):
		"""
		history contains the data for every move of the game, including its current state.
		It is a list of dictionaries, where each index represents the game state after
		each move and where history[0] represents the initial, empty state of the board.
		
		Each entry in history, also known as a game_state, is a dictionary containing three keys:
		chains, nodes and new_stone.
		
		The value of chains is another dictionary, whose keys are integers representing
		each individual chain on the board. The value of each of these keys is a dictionary
		containing two keys: color and liberties. A color of 0 is Black, 1 is White.
		See the example:
		"chains":{1:{"color":1, "liberties":4},
				  2:{"color":0, "liberties":3},
				  3:{"color":0, "liberties":7}
				 }
		
		The value of nodes is a dictionary whose keys are Cartesian coordinates:
		the board is centered at (0,0). The values of each of these keys is a dictionary
		containing three keys: chain_id, neighbors and yinyang. chain_id refers to the chain id the
		node belongs to. If it is a vacant node the value of chain_id is zero. The value of
		neighbors is a list of length 4, where each index is the chain id that the neighboring
		node belongs to. The neighbors in the list are ordered as follows: [N, E, S, W],
		referring to the cardinal directions. If a neighbor node's chain id is listed as -1,
		this means the neighbor node doesn't actually exist and we are on the edge of the board.
		yinyang denotes if the node belongs to black, white, or neither. For more information, see
		the comment below for yinyang_table.
		See the example:
		"nodes":{(-9,-9):{"chain_id":3, "neighbors":[3, 2, -1, -1], "yinyang":1},
				 (-8,-9):{"chain_id":2, "neighbors":[2, 0, -1, 3], "yinyang":3},
				 (-7,-9):{ ... },
				 ...
				}
		
		latest_stone refers to the most recently played stone on the board. The value of latest_stone
		is a list of length 2. At index 0 is a boolean representing the color of the stone, where
		0 = Black and 1 = White. At index 1 is the tuple of Cartesian coordinates where the stone
		was played.
		
		tertiary_string is a representation of the board as a string of tertiary digits.
		"""
		###
		##
		# Build the data structure.
		##
		###
		
		if new_game:
			game_state = {"chains":{},
						  "nodes":{},
						  "latest_stone":[None, None],
						  "tertiary_string": None
						 }
			
			# Initialize the inner nodes.
			for i in range(-8, 9):
				for j in range(-8, 9):
					game_state["nodes"][(i,j)] = {"chain_id" :0, "neighbors":[0, 0, 0, 0], "yinyang":5}
			
			# Initialize the nodes along the North and South edges.
			for i in range(-8, 9):
				game_state["nodes"][(i,9)] = {"chain_id" :0, "neighbors":[-1, 0, 0, 0], "yinyang":5}
				game_state["nodes"][(i,-9)] = {"chain_id" :0, "neighbors":[0, 0, -1, 0], "yinyang":5}
			
			# Initialize the nodes along the East and West edges.
			for j in range(-8, 9):
				game_state["nodes"][(9, j)] = {"chain_id" :0, "neighbors":[0, -1, 0, 0], "yinyang":5}
				game_state["nodes"][(-9, j)] = {"chain_id" :0, "neighbors":[0, 0, 0, -1], "yinyang":5}
			
			# Initialize the nodes at the four corners.
			game_state["nodes"][(9,9)] = {"chain_id" :0, "neighbors":[-1, -1, 0, 0], "yinyang":5}
			game_state["nodes"][(-9,9)] = {"chain_id" :0, "neighbors":[-1, 0, 0, -1], "yinyang":5}
			game_state["nodes"][(-9,-9)] = {"chain_id" :0, "neighbors":[0, 0, -1, -1], "yinyang":5}
			game_state["nodes"][(9,-9)] = {"chain_id" :0, "neighbors":[0, -1, -1, 0], "yinyang":5}
			
			self.history = [game_state]
		
		else:
			# load previous game
			with open(file_path, 'r') as fileObject:
				self.history = pickle.load(fileObject)

		###
		##
		# Build the board image.
		##
		###

		# Define simple board elements
		hbar = unichr(0x2500)
		nt = unichr(0x252c)
		ne_corner = unichr(0x2510)
		et = unichr(0x2524)
		se_corner = unichr(0x2518)
		st = unichr(0x2534)
		sw_corner = unichr(0x2514)
		wt = unichr(0x251c)
		nw_corner = unichr(0x250c)
		cross = unichr(0x253c)
		marker = ' '

		# Define compound board elements
		top_row = nw_corner + hbar + (nt + hbar)*17 + ne_corner + ' \n'
		middle_row = wt + hbar + (cross + hbar)*17 + et + ' \n'
		bottom_row = sw_corner + hbar + (st + hbar)*17 + se_corner + ' \n'
		board_string = top_row + middle_row*17 + bottom_row

		# Use board_string to define the board
		self.board = []
		for i in range(0, len(board_string)):
			self.board.append(board_string[i])

		# Add in referential markers
		self.board[18 + 39*3] = marker #n
		self.board[30 + 39*3] = marker #ne
		self.board[30 + 39*9] = marker #e
		self.board[30 + 39*15] = marker #se
		self.board[18 + 39*15] = marker #s
		self.board[6 + 39*15] = marker #sw
		self.board[6 + 39*9] = marker #w
		self.board[6 + 39*3] = marker # nw
		self.board[18 + 39*9] = marker # origin

		# Lastly, define the players' pieces.
		# NB: This assumes the text is lighter than the background.
		# Reverse the white/black pieces if otherwise.
		self.white_stone = unichr(0x25cf)
		self.black_stone = unichr(0x25cb)

		###
		##
		# define yinyang_table
		##
		###

		# self.yinyang_table is a list of lists to assist in computing the score.
		# Specifically, it is used to determine how a node influences a neighboring
		# node. It uses the yinyang of the node as an index in the yinyang_table,
		# returning a row of the table. The neighboring node's yinyang is an index
		# in that row, returning a value: the neighbor's new yinyang. Below is a
		# graphical representation of this structure.
		#
		# Row headers are the node's yinyang
		# Column headers are the neighboring node's yinyang (pre-influence)
		# Table entry is the neighboring node's yinyang (post-influence)
		#
		# B indicates the node is occupied by a black stone
		# W indicates the node is occupied by a white stone
		# b indicates the node is empty but influenced by black
		# w indicates the node is empty but influenced by white
		# g indicates the node is empty but influenced by both black and white
		# n indicates the node is empty and without known influences
		#
		# _|B_W_b_w_g_n
		# B|B W b g g b
		# W|B W g w g w
		# b|B W b g g b
		# w|B W g w g w
		# g|B W g g g g
		# n|B W b w g n
		#
		# B=0, W=1, b=2, w=3, g=4, n=5
		#
		# _|0_1_2_3_4_5
		# 0|0 1 2 4 4 2
		# 1|0 1 4 3 4 3
		# 2|0 1 2 4 4 2
		# 3|0 1 4 3 4 3
		# 4|0 1 4 4 4 4
		# 5|0 1 2 3 4 5
		#
		self.yinyang_table = [[0,1,2,4,4,2],
							  [0,1,4,3,4,3],
							  [0,1,2,4,4,2],
							  [0,1,4,3,4,3],
							  [0,1,4,4,4,4],
							  [0,1,2,3,4,5]]

		###
		##
		# Other houskeeping items
		##
		###

		self.file_path = file_path
		self.move_number = len(self.history)
		self.pass_count = 0
		self.active_color = 0

	def input_validator(self):
		"""
		Return True if x and y inputs are both ints between -9 and 9.
		False, otherwise.
		"""
		try:
			assert isinstance(self.x, int) and isinstance(self.y, int)
			assert self.x >= -9 and self.x<= 9
			assert self.y >= -9 and self.y <= 9
			return(True)
		
		except AssertionError:
			return(False)


	def is_vacant(self):
		"""
		Returns True if node is vacant, False otherwise.
		"""
		if self.game_state["nodes"][(self.x, self.y)]["chain_id"] == 0:
			return(True)
		
		else:
			return(False)
	
	
	def partition_neighbors(self):
		"""
		Retrieve neighboring chain IDs and categorize them as
		opposite-color chains, same-color chains, or vacant nodes.
		"""
		neighbor_nodes = self.game_state["nodes"][(self.x, self.y)]["neighbors"]
		
		# categorize neighbor chains
		self.opposite_color_chains = []
		self.same_color_chains = []
		self.vacant_nodes = 0
		
		for i in neighbor_nodes:
			
			# Do nothing if it is off an edge
			if i < 0:
				continue
				
			# If empty, increment the vacant node count
			elif i == 0:
				self.vacant_nodes += 1
			
			# If occupied, determine color.
			else:
				color = self.game_state["chains"][i]["color"]
				
				# If same-color...
				if color == self.active_color:
					self.same_color_chains.append(i)
				
				# If opposite-color...
				elif color == 1 - self.active_color:
					self.opposite_color_chains.append(i)
				
				# This should never happen.
				else:
					print("Error ID 1")
		
		# Keep only unique entries
		self.same_color_chains = list(set(self.same_color_chains))
		self.opposite_color_chains = list(set(self.opposite_color_chains))
	
	
	def place_stone(self):
		"""
		Update nodes and chains as a result of a played stone.
		"""
		# Create the stone
		self.game_state["nodes"][(self.x, self.y)]["chain_id"] = self.move_number
		
		# Update the neighbor's neighbor list
		if self.game_state["nodes"][(self.x, self.y)]["neighbors"][0] >= 0:
			self.game_state["nodes"][(self.x, self.y+1)]["neighbors"][2] = self.move_number
		
		if self.game_state["nodes"][(self.x, self.y)]["neighbors"][1] >= 0:
			self.game_state["nodes"][(self.x+1, self.y)]["neighbors"][3] = self.move_number
		
		if self.game_state["nodes"][(self.x, self.y)]["neighbors"][2] >= 0:
			self.game_state["nodes"][(self.x, self.y-1)]["neighbors"][0] = self.move_number
				
		if self.game_state["nodes"][(self.x, self.y)]["neighbors"][3] >= 0:
			self.game_state["nodes"][(self.x-1, self.y)]["neighbors"][1] = self.move_number
		
		# Create the new chain
		self.game_state["chains"][self.move_number] = {"color":self.active_color,
													   "liberties":self.vacant_nodes}
		
		# Reduce the liberties by 1 of all opposite-color, neighbor chains.
		for i in self.opposite_color_chains:
			self.game_state["chains"][i]["liberties"] -= 1
	
	
	def merge_chains(self):
		"""
		Merge chains that are now connected as a result of the latest stone.
		"""
		keep_chain_id = min(self.same_color_chains)
		self.same_color_chains.remove(keep_chain_id)
		remove_chain_ids = self.same_color_chains
		remove_chain_ids.append(self.move_number)
		
		# Update nodes
		for node_dict in self.game_state["nodes"].values():
		
			if node_dict["chain_id"] in remove_chain_ids:
				node_dict["chain_id"] = keep_chain_id
				
			for i in range(0,4):
			
				if node_dict["neighbors"][i] in remove_chain_ids:
					node_dict["neighbors"][i] = keep_chain_id
					
		# Remove old chain IDs
		for chain_id in remove_chain_ids:
			self.game_state["chains"].pop(chain_id)
		
		# Update liberties for the merged chain.
		liberties = 0
		
		for node_dict in self.game_state["nodes"].values():
		
			if node_dict["chain_id"] == 0 and keep_chain_id in node_dict["neighbors"]:
				liberties += 1
		
		self.game_state["chains"][keep_chain_id]["liberties"] = liberties

	
	def clear_dead_chains(self):
		"""
		This method will identify and remove dead chains. If a dead
		chain contains the new stone, it will not be removed yet, since 
		the removal of other chains may provide some liberties.
		"""
		self.new_stone_chain_id = self.game_state["nodes"][(self.x, self.y)]["chain_id"]
		all_chains = self.game_state["chains"].items()
		chains_to_clear = []
		
		# Determine which chains are dead and remove them.
		for chain_tuple in all_chains:
			
			if chain_tuple[1]["liberties"] == 0 and chain_tuple[0] != self.new_stone_chain_id:
				chains_to_clear.append(chain_tuple[0])
				self.game_state["chains"].pop(chain_tuple[0])
		
		# For every node...
		for node_dict in self.game_state["nodes"].values():
			
			# First, clear out the dead chain IDs from neighbor lists
			# NB this happens first to avoid a bug.
			for i in range(0,4):
				
				if node_dict["neighbors"][i] in chains_to_clear:
					node_dict["neighbors"][i] = 0
			
			# Now remove any dead chain IDs
			if node_dict["chain_id"] in chains_to_clear:
				node_dict["chain_id"] = 0
				unique_neighbor_chain_ids = list(set(node_dict["neighbors"]))
				# NB the above list will not contain any entries of dead chains 
				# since they were removed in the step above.
				
				try:
					unique_neighbor_chain_ids.remove(0)
				except:
					pass
				
				try:
					unique_neighbor_chain_ids.remove(-1)
				except:
					pass
					
				# Increase neighbor chains' liberties by 1
				for neighbor_chain_id in unique_neighbor_chain_ids:
					self.game_state["chains"][neighbor_chain_id]["liberties"] += 1
	
	
	def tertiate(self):
		"""
		This will represent the game_state as a tertiary string. Every
		digit represents a unique node on the board. A 0 indicates it is
		occupied by a black stone, a 1 indicates it is occupied by a 
		white stone, and a 2 indicates it is vacant.
		
		This representation is necessary to check the Ko Rule. 
		We cannot simply compare the current game_state with the
		state at history[-2] because even if the stone positions are 
		identical, the chain IDs may differ and so the test could fail to
		detect a Ko violation. Representing the board as a tertiary string
		gets around this problem.
		
		Additionally, this representation is much more efficient for 
		rendering the board image.
		"""
		tertiary_string = ""
		
		for j in range(9, -10, -1):
		
			for i in range(-9, 10):
			
				id = self.game_state["nodes"][(i, j)]["chain_id"]
				
				# If there is no stone in this position...
				if id == 0:
					tertiary_string += "2"
				
				else:
					color = self.game_state["chains"][id]["color"]
					tertiary_string += str(color)
					
		self.game_state["tertiary_string"] = tertiary_string
	
	
	def finish_turn(self):

		self.game_state["latest_stone"] = [self.active_color, (self.x, self.y)]
		self.history.append(self.game_state)
		self.active_color = 1 - self.active_color
		self.move_number += 1
		self.pass_count = 0
	
	
	def image_generator(self, tertiary_string):
		"""
		Compute the board image from the tertiary string.
		"""

		# We don't modify self.board since this method never erases
		# any stones. Hence, self.board will always represent the empty
		# board which we will copy from each time.
		board = self.board[:]
		
		for row in range(0, 19):
			for column in range(0, 19):
				
				if tertiary_string[19*row + column] == '0':
					board[39*row + 2*column] = self.black_stone
					board[39*row + 2*column + 1] = ' '

				elif tertiary_string[19*row + column] == '1':
					board[39*row + 2*column] = self.white_stone

				else:
					pass

		print (''.join(board))
		

class Go(GoParent):


	def __init__(self, file_path, new_game=True):
		super(Go, self).__init__(file_path, new_game)
	
	
	def play(self, x, y):
		"""
		This method executes a move.
		"""
		
		self.x = x
		self.y = y
		self.game_state = deepcopy(self.history[-1])
		
		# If the input is invalid, do not execute the move.
		valid = self.input_validator()
		if not valid:
			print("\nInvalid input")
			return
		
		# If node is not vacant, do not execute the move.
		vacant = self.is_vacant()
		if not vacant:
			print("\nSpace already occupied")
			return
		
		# Perform neighborhood discovery.	
		self.partition_neighbors()
		
		# Finally ready to place the stone
		self.place_stone()
		
		# If there are same-color, neighbor chains then merge.
		if len(self.same_color_chains) > 0:
			self.merge_chains()

		# Remove any chains that may have been killed.
		self.clear_dead_chains()
		
		# Do not allow suicidal moves.
		if self.game_state["chains"][self.new_stone_chain_id]["liberties"] == 0:
			print("\nSuicide is not permitted.")
			return
		
		# Convert the game state to a string
		self.tertiate()
		
		# Test the Ko Rule
		try:
			if self.game_state["tertiary_string"] == self.history[-2]["tertiary_string"]:
				print("\nThis violates the 'Ko rule'")
				return
		
		except:
			pass
		
		# The move is finally determined to be valid.
		self.finish_turn()
		self.save()
		self.display()
	
	
	def pass_turn(self):
		
		self.pass_count += 1
		
		if self.pass_count == 1:
			print("%s has passed." % ["Black", "White"][self.active_color])
			self.active_color = 1 - self.active_color
		
		else:
			self.end_game()
	
	
	def display(self, index = -1):
		"""
		This method draws the game board after any given move, and
		provides info on that move.
		"""
		try:
			tertiary_string = self.history[index]["tertiary_string"]
				
		except Exception as exc:
			print("No such index.")
			
		else:
			color = self.history[index]["latest_stone"][0]
			node = self.history[index]["latest_stone"][1]
			
			if index >= 0:
				moves = index
			
			else:
				moves = len(self.history) + index
				
			print("\n%s just moved at %r" % (["Black", "White"][color], node))
			print("Total moves executed: %d\n" % moves)
			self.image_generator(tertiary_string)
	
	
	def revert(self, index=-1):
		"""
		Permanently rewind the game to a given move.
		"""
		#resp = raw_input("Are you sure you want to revert the game to move %d? (y/n)" % index)
		print("Are you sure you want to revert the game to move %d? (y/n)" % index)
		resp='y'
		if resp == 'y':
			
			if index >= 0:
				max_index = index + 1
			
			else:
				max_index = len(self.history) + index + 1

			print("Reverting game back to move %d.\n" % index)
			self.history = deepcopy(self.history[0:max_index])
			self.display()

		elif resp == 'n':
			pass

		else:
			print("Invalid response\n")
	
	
	def save(self, index=None, file_path=None):
		"""
		This will save the game as a pickle object.

		You can save the game to a different location than where the game
		was initialized. This is done by providing a new file_path.

		You can also save the game at any given move (index).
		"""
		if file_path == None:
			file_path = self.file_path

		if index != None:

			if index >= 0:
				max_index = index + 1
			
			else:
				max_index = len(self.history) + index + 1

			obj = deepcopy(self.history[0:max_index])

		else:
			obj = self.history

		with open(file_path, 'w') as fileObject:
			fileObject.truncate()
			pickle.dump(obj, fileObject)
	
	
	def score(self, index=-1):
		
		# First, spread influence back and forth across the boards, updating yinyangs
		nodes = self.history[index]["nodes"]
		modified = True
		counter = 1

		# stop spreading influence if nothing has changed since the last pass
		while modified == True:
			modified = False
			# influence spreads from top left to bottom right, and then in reverse.
			# The direction is determined by posneg which is always either +1 or -1.
			posneg = (counter%2)*2-1

			for i in range(-9*posneg, 10*posneg, posneg):
				for j in range(-9,10):
					# snag this node's yinyang
					node_yy = nodes[(i,j)]["yinyang"]
					
					# update the neighbor's yinyangs
					for k in range(0,4):
						x = i + (k%2)*(2-k)
						y = j + ((k+1)%2)*(1-k)
						
						if x>-10 and x<10 and y>-10 and y<10:
							neigh_yy = nodes[(x,y)]["yinyang"]
							new_yy = self.yinyang_table[node_yy][neigh_yy]
							
							if new_yy != neigh_yy:
								modified = True
								nodes[(x,y)]["yinyang"] = new_yy
		
		# Second, compute score from the yinyangs
		black_score = 0
		white_score = 0

		for value in nodes.values():
			yy = value["yinyang"]

			if yy == 0 or yy == 2:
				black_score += 1

			elif yy == 1 or yy == 3:
				white_score += 1

		print "Black: %d" % black_score
		print "White: %d" % white_score


	def end_game(self):
		
		print "Ending game. The final score is:"
		self.score()
		try:
			exit(0)
		except SystemExit:
			pass


if __name__ == "__main__":

	file_path = os.path.join(os.getcwd(), "game_pickle.txt")
	game = Go(file_path, True)
	
	#need to write a test case for the scoring mechanism.
	
	if not True:
		game.play(0,2)
		game.play(0,9)
		game.play(3,4)
		print "***Now to show the game board back a move***\n\n"
		game.display(-2)
		game.revert(2)
		game.play(4,3)
		game.end_game()

	if not True:
		game.play(0,0)
		game.play(0,9)
		print "opening a new game where the last left off..."
		print "executing the next move"
		new_game = Go(file_path, False)
		new_game.play(9,0)
		new_game.play(0,-9)
		new_game.play(-9,0)
		print "Opening a newer game back a move..."
		print "The current state is:"
		new_game.save(history_index=-2, file_path="/home/jn/GitLab/Go/game_pickle2.txt")
		newest_game = Go("/home/jn/GitLab/Go/game_pickle2.txt", False)
		newest_game.display()

	if not True:
		game.play(-1, 0) #Black
		game.play(2, 0)
		game.play(0, 1) #Black
		game.play(1, 1)
		game.play(0, -1) #Black
		game.play(1, -1)
		game.play(1, 0) #Black
		game.play(0 ,0) #This isn't suicide!!
		game.play(1, 0) #Black     # violates Ko rule!
		game.play(4, 4) #Black tries again
	
	if not True:
		game.play(-9,-8)
		game.play(0,0)
		game.play(-8,-9)
		game.play(1,0)
		game.play(-8,-8)
		game.play(-9,-9)
		game.play(1,0)
		game.play(1,2)
		
	
	if not True:
		game.play(-9,-8)
		game.play(-9,-9)
		print(game.history[-1]["chains"])
		print(game.history[-1]["nodes"][-9,-8])
		print(game.history[-1]["nodes"][-9,-9])
		game.play(-8,-9)
		print(game.history[-1]["chains"])
		print(game.history[-1]["nodes"][-9,-8])
		print(game.history[-1]["nodes"][-9,-9])
		print(game.history[-1]["nodes"][-8,-9])
		game.play(0,0)
		game.play(-8,-8)
		print(game.history[-1]["chains"])
		print(game.history[-1]["nodes"][-9,-8])
		print(game.history[-1]["nodes"][-9,-9])
		print(game.history[-1]["nodes"][-8,-9])
		print(game.history[-1]["nodes"][-8,-8])
		print(game.history[-1]["nodes"][0,1])
	
	if not True:
		game.play(-9,-8)
		game.play(0,0)
		game.play(-9,-9)
		print(game.history[-1]["chains"])
		print(game.history[-1]["nodes"][-9,-8])
		print(game.history[-1]["nodes"][-9,-9])
		print(game.history[-1]["nodes"][0,1])
	
